PDF = xelatex
MODE = --interaction batchmode

DOC = proba

pdf: $(DOC).tex
	$(PDF) $(MODE) $(DOC).tex
	#rm *.out *.aux *.log

clean:
	#rm *.pdf
	rm *.out *.aux *.log *.toc
